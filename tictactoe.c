#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* 
 * This is my attempt to create a tic-tac-toe game in C. 
 * Written by Torsten Diesel, Licenced GPL v3+
 */

int Square[9]; 

/*
 * These variables will be assigned 0, 1, or 4 for whether they are blank, X's, or O's respectively. 
 * They will be placed in these locations:
 *  0 | 1 | 2 
 * ---+---+---
 *  3 | 4 | 5 
 * ---+---+---
 *  6 | 7 | 8 
 */

int DrawBoard() {
    /* Bug: Currently prints it as 0, 1, or 4 instead of X's and O's */
    printf("\n");
    printf(" %d | %d | %d  \n", Square[0], Square[1], Square[2]);
    printf("---+---+---\n");
    printf(" %d | %d | %d  \n", Square[3], Square[4], Square[5]);
    printf("---+---+---\n");
    printf(" %d | %d | %d  \n\n", Square[6], Square[7], Square[8]);
}

/* finished */
int PlayerMove() {
  int CurrentPlayerMove;
  printf(" 0 | 1 | 2 \n---+---+---\n 3 | 4 | 5 \n---+---+---\n 6 | 7 | 8 \n\n");
  printf("Where do you want to place an X?\t> ");
  scanf("%i", &CurrentPlayerMove);
  printf("You placed an X on Square %d.\n", CurrentPlayerMove);
  if (Square[CurrentPlayerMove] == 0) {
    Square[CurrentPlayerMove] = 1;
    DrawBoard();
  } else {
    printf("There's already a mark there.\n");
    PlayerMove();
  }
}


int CheckSquares() {

  int Group[8], GroupSum[8];
  Group[0] = Square[0], Square[1], Square[2];
  Group[1] = Square[3], Square[4], Square[5];
  Group[2] = Square[6], Square[7], Square[8];
  Group[3] = Square[0], Square[3], Square[6];
  Group[4] = Square[1], Square[4], Square[7];
  Group[5] = Square[2], Square[5], Square[8];
  Group[6] = Square[0], Square[4], Square[8];
  Group[7] = Square[2], Square[4], Square[6];
  
  /* Rows */
  GroupSum[0] = Square[0] + Square[1] + Square[2];
  GroupSum[1] = Square[3] + Square[4] + Square[5];
  GroupSum[2] = Square[6] + Square[7] + Square[8];
  /* Columns */
  GroupSum[3] = Square[0] + Square[3] + Square[6];
  GroupSum[4] = Square[1] + Square[4] + Square[7];
  GroupSum[5] = Square[2] + Square[5] + Square[8];
  /* Diagonals */
  GroupSum[6] = Square[0] + Square[4] + Square[8];
  GroupSum[7] = Square[2] + Square[4] + Square[6];

  int i, PlayerWinPossible, ComputerWinPossible, PlayerWin, ComputerWin;
  for (i = 0; i <=7; i = i + 1) {
    if (GroupSum[i] == 2) {
      PlayerWinPossible = i;
    } if (GroupSum[i] == 8) {
      ComputerWinPossible = i;
    } if (GroupSum[i] == 3) {
      PlayerWin = 1;
    } if (GroupSum[i] == 12) {
      ComputerWin = 1;
    }
  }
}




int PlayerWinPossibleP() {
  CheckSquares();
  int ii;
  for (ii = 0; ii <= 8; ii = ii+1) {
    if (GroupSum[i] == 2) {
      PlayerWinPossible = i;
    } else {
      PlayerWinPossible = 9;
    }
  }
} int ComputerWinPossibleP() {
  CheckSquares();  
  for (ii = 0; ii <= 8; ii = ii+1) {
    if (GroupSum[i] == 8) {
      ComputerWinPossible = i;
    } else {
      ComputerWinPossible = 9;
    }
  }
} int PlayerWinP() {
  CheckSquares();  
  for (ii = 0; ii <= 8; ii = ii+1) {
    if (GroupSum[i] == 3) {
      PlayerWin = 1;
    } else {
      PlayerWin = 0;
    }
  }
} int ComputerWinP() {
  CheckSquares();  
  for (ii = 0; ii <= 8; ii = ii+1) {
    if (GroupSum[i] == 12) {
      ComputerWin = 1;
    } else {
      ComputerWin = 0;
    }
  }
  

int ComputerMove() {
  int i;
  CheckSquares();
  
  if (ComputerWinPossible != 9) {
    for (i = 0; i <= 2; i = i + 1) {
      Group[i,ComputerWinPossible] = 4; 
    }
  }
   
    
  
}

int IsBoardFull() {
  if ((Square[0] && Square[1] && Square[2] && Square[3] && Square[4] && Square[5] && Square[6] && Square[7] && Square[8]) != 1) {
    return 0;
  }
}

int IsGameOver() {
  CheckSquares();
  int BoardFull;
  if (ComputerWin == 1) {
    printf("You lose!\n");
    return 1;
  } if (PlayerWin == 1) {
    printf("You win!\n");
    return 1;
  } if (IsBoardFull() == 1) {
    printf("Board full, Game over! \n");
    return 1;
    } else {
	    return 0;
  }
}

int main() {

  printf("Tic Tac Toe!\n\n");
  while (IsGameOver() != 1) {
    PlayerMove();
    if (IsGameOver() == 1) {
      break;
    }
    ComputerMove();
  }
}

