# Tic Tac Toe
This is a simple, generic command-line tic tac toe program. Nothing too fancy. It's written in C, and licenced as GPL v3+. It currently doesn't meet the goals, you're welcome to contribute. Once it does meet the goals, I don't think i'll continue working on it, there's only so much you can add to a tic-tac-toe program. 

## Goals

- Player can move
- There's a simple AI
- Squares that are filled in show X or O, otherwise a number (for player input)

## Compiling and running

I didn't plan for it to be run anywhere but a GNU/Linux system, but you're welcome to try to compile it anywhere. 

### On linux: 

Clone the project:

    git clone https://gitlab.com/DreadPirateRobot/tic-tac-toe.git

cd to the directory where you cloned it to, then just run

    gcc tictactoe.c -o TicTacToe

then to run it, stay in that directory and just run

    ./TicTacToe
